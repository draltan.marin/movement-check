# Balance check service

Bank movement check against monthly balances.

## Movement validations

In order to check movement consistency against balances movements are categorised in periods. The actual periods are intervals between balance dates (start exclusive, end inclusive).

Periods can be empty if no transactions performed during the corresponding dates.

The `reasons` response is structured as follows:
```
{
  duplicateMovements: [issue],
  periods: [
    {
      "periodStart": "2021-01-31T00:00:00.000Z",
      "periodEnd": "2021-02-28T00:00:00.000Z",
      "issueList": [issue]
    }
  ]
}
```

We assume that balances tell are correct and complete. Movement are checked for duplicate or inconsistent ids (same id for different payload).

- Movement consistency checks:
  - `DUPLICATE_MOVEMENTS`
    ```
    {
      "type": "DUPLICATE_MOVEMENTS",
      "message": "Duplicate movements",
      "payload": {
        "date": "2021-02-01T00:00:00.000Z",
        "id": 3,
        "amount": 20,
        "count": 2
      }
    }
    ```

  - `INCONSISTENT_MOVEMENT_ID`
    ```
    {
      "type": "INCONSISTENT_MOVEMENT_ID",
      "message": "Multiple variants found for the same movement id",
      "payload": [
        {
          "count": 1,
          "movement": {
            "date": "2021-01-07T00:00:00.000Z",
            "id": 2,
            "amount": 20
          }
        },
        {
          "count": 1,
          "movement": {
            "date": "2021-01-07T00:00:00.000Z",
            "id": 2,
            "amount": 30
          }
        }
      ]
    },
    ```
Each identified period is checked for balance consistency. This is, we expect the initial balance + total amount of movements to be the final balance.

- Period consistency checks:
  - `NO_INITIAL_BALANCE`
  - `NO_FINAL_BALANCE`
  - `BALANCE_MISMATCH`

If no initial balance we assume it to be 0 but an issue is emitted.

```
{
  "type": "NO_INITIAL_BALANCE",
  "message": "No initial balance found for period. Assuming zero as initial balance."
}
```

If no final balance we are unable to verify that the movements are correct. An issue is triggered as well.

Example:

```
{
  "type": "NO_FINAL_BALANCE",
  "message": "No final balance found for period. Unable to check final balance.",
  "payload": {
    "initialBalance": 90,
    "movementsTotal": 40,
    "expectedFinalBalance": 130
  }
}
```

In case of balance mismatch an additional payload is generated to further explore the issue:
 - `initialBalance`
 - `movementsTotal`
 - `expectedFinalBalance`
 - `actualFinalBalance`
 - `missing`: `actualFinalBalance` - `expectedFinalBalance`

Example:
```
{
  "type": "BALANCE_MISMATCH",
  "message": "Period balance mismatch",
  "payload": {
    "initialBalance": 100,
    "movementsTotal": 40,
    "expectedFinalBalance": 140,
    "actualFinalBalance": 120,
    "missing": -20
  }
}
```

## Demo

Launch the server (see usage section below) and try the following commands: 

```
curl -X POST http://localhost:8080/movements/validation -d @demo/validSample.json --header "Content-Type: application/json"
```

```
curl -X POST http://localhost:8080/movements/validation -d @demo/invalidSample.json --header "Content-Type: application/json"
```

## Usage

Install dependencies

```sh
npm install
```

Launch server

```sh
npm start
```

Development mode (inspect and auto reload on changes):

```sh
npm run watch
```

Tests:

```sh
npm run test
```

Test development mode:

```sh
npm run test-watch
```
