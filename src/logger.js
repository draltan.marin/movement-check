import winston from 'winston';
import config from './config.js';

const logger = winston.createLogger({
  format: winston.format.cli(),
  transports: [
    new winston.transports.Console({ level: config.logLevel}),
  ]
});

logger.stream = {
  write: (message, ) => {
    logger.info(message.trim());
  }
};

export default logger;
