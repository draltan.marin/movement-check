import express from 'express';
import { checkBalances, hasErrors } from './movementCheck.js';

const api = express.Router();

api.post('/validation', (req, res) => {
  let {movements, balances} = req.body;
  let {skipDuplicates} = req.query;

  if (movements === undefined || balances === undefined) {
    res.status(401).json({
      message: `Request should contain an array of movements and an array of balances. Got ${JSON.stringify({movements, balances})}`
    });
  }

  let reasons = checkBalances({
    movements,
    balances,
    skipDuplicates
  });

  if (hasErrors(reasons)) {
    return res.status(418).json({
      message: 'I\'m a teapot',
      reasons,
    });
  }
  return res.status(202).json({
    message: 'Accepted',
  });
});

export default api;
