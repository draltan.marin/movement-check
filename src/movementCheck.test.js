import * as movementCheck from './movementCheck.js';

describe('period state management', () => {
  it('should start new period on previous period\'s end', () => {
    let periods = movementCheck.periodBuilder();

    periods.closePeriod({date: new Date()});
    periods.closePeriod({date: new Date()});
    periods.closePeriod({date: new Date()});
    periods.closePeriod({date: new Date()});

    let resultingPeriods = periods.getPeriods();

    let previousEnd = undefined;
    resultingPeriods.forEach(period => {
      let {start, end, movements} = period;
      if (previousEnd) {
        expect(start.date).toBe(previousEnd.date);
      }
      expect(movements.length).toBe(0);
      previousEnd = end;
    });
  });

  it('should keep initial movements in an "undefined start" period', () => {
    let periods = movementCheck.periodBuilder();

    periods.addMovement({date: new Date()});
    periods.addMovement({date: new Date()});
    periods.addMovement({date: new Date()});
    periods.closePeriod({date: new Date()});

    let resultingPeriods = periods.getPeriods();

    expect(resultingPeriods.length).toBe(1);
    let initialPeriod = resultingPeriods[0];
    expect(initialPeriod.movements.length).toBe(3);
    expect(initialPeriod.start).toBeUndefined();
    expect(initialPeriod.end).not.toBeUndefined();
  });

  it('should keep final movements in an "undefined end" period', () => {
    let periods = movementCheck.periodBuilder();

    periods.closePeriod({date: new Date()});
    periods.addMovement({date: new Date()});
    periods.addMovement({date: new Date()});
    periods.addMovement({date: new Date()});

    let resultingPeriods = periods.getPeriods();

    expect(resultingPeriods.length).toBe(1);
    let initialPeriod = resultingPeriods[0];
    expect(initialPeriod.movements.length).toBe(3);
    expect(initialPeriod.start).not.toBeUndefined();
    expect(initialPeriod.end).toBeUndefined();
  });

  it('should avoid "empty" undefined periods', () => {
    // avoid having periods with either undefined start or end that don't count any movement
    let periods = movementCheck.periodBuilder();

    periods.closePeriod({date: new Date()});
    periods.addMovement({date: new Date()});
    periods.addMovement({date: new Date()});
    periods.addMovement({date: new Date()});
    periods.closePeriod({date: new Date()});

    let resultingPeriods = periods.getPeriods();

    expect(resultingPeriods.length).toBe(1);
    let initialPeriod = resultingPeriods[0];
    expect(initialPeriod.movements.length).toBe(3);
    expect(initialPeriod.start).not.toBeUndefined();
    expect(initialPeriod.end).not.toBeUndefined();
  });

  it('should keep "empty" but defined periods', () => {
    // avoid having periods with either undefined start or end that don't count any movement
    let periods = movementCheck.periodBuilder();

    periods.closePeriod({date: new Date()});
    periods.closePeriod({date: new Date()});

    let resultingPeriods = periods.getPeriods();

    expect(resultingPeriods.length).toBe(1);
    let initialPeriod = resultingPeriods[0];
    expect(initialPeriod.movements.length).toBe(0);
    expect(initialPeriod.start).not.toBeUndefined();
    expect(initialPeriod.end).not.toBeUndefined();
  });
});

describe('period grouping', () => {
  it('should group movements into non overlapping contiguous periods', () => {
    let movements = [
      {id: 1, date: new Date('2020-10-05'), amount: 10},
      {id: 2, date: new Date('2021-01-07'), amount: 20},
      {id: 3, date: new Date('2021-02-01'), amount: 20},
      {id: 4, date: new Date('2021-03-15'), amount: -30},
      {id: 5, date: new Date('2021-04-08'), amount: 40},
    ];

    let balances = [
      {date: new Date('2021-03-31'), balance: 90},
      {date: new Date('2021-01-31'), balance: 100},
      {date: new Date('2021-02-28'), balance: 120},
    ];

    let periods = movementCheck.resolvePeriods({movements, balances});

    expect(periods).not.toBeUndefined();
    expect(periods.length).toBeGreaterThanOrEqual(balances.length - 1);

    let previousEnd;
    periods.forEach(period => {
      let {start, end, movements: movs} = period;

      // N-1's end === N's start
      if (previousEnd !== undefined) {
        expect(start).not.toBeUndefined();
        expect(previousEnd.date).toBe(start.date);
      }

      // start <= end
      if (start !== undefined && end !== undefined) {
        expect(end.date - start.date >= 0).toBe(true);
      }

      // all movement dates are in between periods' start and end
      movs.forEach(movement => {
        if (start) {
          expect(movement.date - start.date > 0).toBe(true);
        }
        if (end) {
          expect(end.date - movement.date >= 0).toBe(true);
        }
      });

      // update next round's previousEnd
      if (end !== undefined) {
        previousEnd = end;
      }
    });
  });

  it('should create a single period when no balances', () => {
    let movements = [
    ];

    let balances = [
      {date: new Date('2021-03-31'), balance: 90},
      {date: new Date('2021-01-31'), balance: 100},
      {date: new Date('2021-02-28'), balance: 120},
    ];

    let periods = movementCheck.resolvePeriods({movements, balances});

    expect(periods.length).toBe(2);
    periods.forEach(period => {
      expect(period.movements.length).toBe(0);
    });
  });

  it('should create periods even when no movements', () => {
    let movements = [
      {id: 3, date: new Date('2021-02-01'), amount: 20},
      {id: 2, date: new Date('2021-01-07'), amount: 20},
      {id: 1, date: new Date('2020-10-05'), amount: 10},
      {id: 5, date: new Date('2021-04-08'), amount: 40},
      {id: 4, date: new Date('2021-03-15'), amount: -30},
    ];

    let balances = [
    ];

    let periods = movementCheck.resolvePeriods({movements, balances});

    expect(periods.length).toBe(1);
    expect(periods[0].movements.length).toBe(movements.length);
  });

  it('should put each movement in a single group', () => {
    let movements = [
      {id: 3, date: new Date('2021-02-01'), amount: 20},
      {id: 2, date: new Date('2021-01-07'), amount: 20},
      {id: 1, date: new Date('2020-10-05'), amount: 10},
      {id: 5, date: new Date('2021-04-08'), amount: 40},
      {id: 4, date: new Date('2021-03-15'), amount: -30},
    ];

    let balances = [
      {date: new Date('2021-03-31'), balance: 90},
      {date: new Date('2021-01-31'), balance: 100},
      {date: new Date('2021-02-28'), balance: 120},
    ];

    let periods = movementCheck.resolvePeriods({movements, balances});

    let resultMovements = periods.reduce((acc, period) => {
      return acc.concat(period.movements);
    }, []);

    // the number of movements is preserved
    expect(resultMovements.length).toBe(movements.length);

    // each initial movement exist in the final result
    let resultingIds = resultMovements.map(movement => movement.id);
    movements.forEach(movement => {
      expect(resultingIds.includes(movement.id)).toBe(true);
    });
  });
});

describe('movement aggregation', () => {
  it('should compute correct totals', () => {
    let testData = [{
      movements: [
        {amount: 10},
        {amount: 40},
        {amount: 30},
        {amount: .5},
      ],
      expected: 80.5
    },{
      movements: [
        {amount: 10},
        {amount: -40},
        {amount: 30},
        {amount: .5},
      ],
      expected: 0.5
    },{
      movements: [],
      expected: 0
    },
    ];

    testData.forEach(({movements, expected}) => {
      let total = movementCheck.aggregate(movements);
      expect(total).toBe(expected);
    });
  });
});

describe('duplicate movement checks', () => {
  it('should detect duplicates based on ids', () => {
    let movements = [
      {id: 1},
      {id: 5},
      {id: 1},
    ];
    let duplicateReport = movementCheck.detectDuplicates(movements);
    expect(duplicateReport.length).toBe(1);
  });

  it('should report as many duplicates as duplicate ids found', () => {
    let movements = [
      {id: 1},
      {id: 5},
      {id: 1},
      {id: 5},
      {id: 1},
      {id: 4},
      {id: 3},
      {id: 3},
    ];
    let duplicateReport = movementCheck.detectDuplicates(movements);
    // 1, 5, 3
    expect(duplicateReport.length).toBe(3);
  });

  it('should report as many duplicates as duplicate ids found', done => {
    let movements = [
      {id: 1},
      {id: 5},
      {id: 1},
      {id: 5},
      {id: 1},
      {id: 4},
      {id: 3},
      {id: 3.0}, // should consider ids as numbers
    ];
    let duplicateReport = movementCheck.detectDuplicates(movements);
    // 1, 5, 3
    expect(duplicateReport.length).toBe(3);
    duplicateReport.forEach(issue => {
      let {type, payload} = issue;
      expect(type).toBe('DUPLICATE_MOVEMENTS');
      switch (payload.id) {
      case 1:
        expect(payload.count).toBe(3);
        break;
      case 5:
        expect(payload.count).toBe(2);
        break;
      case 3:
        expect(payload.count).toBe(2);
        break;
      default:
        done('should not have found any other case');
      }
    });
    done();
  });

  it('should return empty report if no duplicates', () => {
    let movements = [
      {id: 1},
      {id: 5},
      {id: 4},
      {id: 3},
    ];
    let duplicateReport = movementCheck.detectDuplicates(movements);
    expect(duplicateReport.length).toBe(0);
  });

  it('should detect possibly wrong movement id', () => {
    let movements = [
      {id: 1, amount: 20},
      {id: 1, amount: 10},
      {id: 1, amount: 10},
      {id: 4},
      {id: 3},
    ];
    let duplicateReport = movementCheck.detectDuplicates(movements);
    expect(duplicateReport.length).toBe(1);
    let {type, payload} = duplicateReport[0];
    expect(type).toBe('INCONSISTENT_MOVEMENT_ID');
    expect(payload.length).toBe(2); // two variants expected
  });
});

describe('ignore duplicates', () => {
  it('should keep one movement instance per variant', () => {
    let movements = [
      {id: 1, amount: 20},
      {id: 1, amount: 10},
      {id: 1, amount: 10},
      {id: 4},
      {id: 3},
    ];
    let cleanedMovments = movementCheck.ignoreDuplicates(movements);
    expect(cleanedMovments.length).toBe(4);
  });
});

describe('full report', () => {
  it('should provide an empty report when all ok', () => {
    let movements = [
      {id: 1, date: '2020-10-05', amount: 10},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 3, date: '2021-02-01', amount: 20},
      {id: 4, date: '2021-03-31', amount: -30},
      {id: 5, date: '2021-04-08', amount: 40},
    ];

    let balances = [
      {date: '2020-01-31', balance: 70},
      {date: '2021-03-31', balance: 90},
      {date: '2021-01-31', balance: 100},
      {date: '2021-02-28', balance: 120},
      {date: '2021-05-31', balance: 130},
    ];

    let {duplicateMovements, periods} = movementCheck.checkBalances({movements, balances});

    expect(duplicateMovements.length).toBe(0);
    expect(periods.length).toBe(0);
  });

  it('should detect intermediate errors even if final balance ok', () => {
    let movements = [
      {id: 1, date: '2020-10-05', amount: 10},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 3, date: '2021-02-01', amount: 20},
      {id: 4, date: '2021-04-01', amount: -30}, // this one moved to the next period
      {id: 5, date: '2021-04-08', amount: 40},
    ];

    let balances = [
      {date: '2020-01-31', balance: 70},
      {date: '2021-03-31', balance: 90},
      {date: '2021-01-31', balance: 100},
      {date: '2021-02-28', balance: 120},
      {date: '2021-05-31', balance: 130},
    ];

    let {duplicateMovements, periods} = movementCheck.checkBalances({movements, balances});

    expect(duplicateMovements.length).toBe(0);
    expect(periods.length).toBe(2);
  });

  it('should detect duplicate movements', () => {
    let movements = [
      {id: 1, date: '2020-10-05', amount: 10},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 3, date: '2021-02-01', amount: 20},
      {id: 4, date: '2021-03-15', amount: -30},
      {id: 4, date: '2021-03-15', amount: -30},
      {id: 5, date: '2021-04-08', amount: 40},
    ];

    let balances = [
      {date: '2020-01-31', balance: 70},
      {date: '2021-03-31', balance: 90},
      {date: '2021-01-31', balance: 100},
      {date: '2021-02-28', balance: 120},
      {date: '2021-05-31', balance: 130},
    ];

    let {duplicateMovements, periods} = movementCheck.checkBalances({movements, balances});

    expect(duplicateMovements.length).toBe(1);
    // it should affect a single period 2021-02-28 to 2021-03-31
    expect(periods.length).toBe(1);
  });

  it('should ignore duplicates for period checks only', () => {
    let movements = [
      {id: 1, date: '2020-10-05', amount: 10},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 3, date: '2021-02-01', amount: 20},
      {id: 4, date: '2021-03-15', amount: -30},
      {id: 4, date: '2021-03-15', amount: -30},
      {id: 5, date: '2021-04-08', amount: 40},
    ];

    let balances = [
      {date: '2020-01-31', balance: 70},
      {date: '2021-03-31', balance: 90},
      {date: '2021-01-31', balance: 100},
      {date: '2021-02-28', balance: 120},
      {date: '2021-05-31', balance: 130},
    ];

    let {duplicateMovements, periods} = movementCheck.checkBalances({movements, balances, skipDuplicates: true});

    expect(duplicateMovements.length).toBe(2);
    // ignoring duplicates all should be great again
    expect(periods.length).toBe(0);
  });

  it('should detect inconsistent balances', () => {
    let movements = [
      {id: 1, date: '2020-10-05', amount: 10},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 3, date: '2021-02-01', amount: 20},
      {id: 3.1, date: '2021-02-03', amount: 20},
      {id: 4, date: '2021-03-15', amount: -30},
      {id: 5, date: '2021-04-08', amount: 40},
    ];

    let balances = [
      {date: '2020-01-31', balance: 70},
      {date: '2021-03-31', balance: 90},
      {date: '2021-01-31', balance: 100},
      {date: '2021-02-28', balance: 120},
      {date: '2021-05-31', balance: 130},
    ];

    let {duplicateMovements, periods} = movementCheck.checkBalances({movements, balances, skipDuplicates: true});

    expect(duplicateMovements.length).toBe(0);
    expect(periods.length).toBe(1);
  });
});
