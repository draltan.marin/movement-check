import express from 'express';
import bodyParser from 'body-parser';
import api from './api.js';

const app = express();

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.sendStatus(200);
});

app.use('/movements', api);

app.use('*', (req, res) => {
  res.sendStatus(404);
});

export default app;
