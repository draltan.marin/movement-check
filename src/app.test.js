import app from './app';
import request from 'supertest';

describe('app basics', () => {
  it('should be alive', done => {
    request(app)
      .get('/')
      .send()
      .expect(200, done);
  });

  it('should not find any random route', done => {
    request(app)
      .get('/randomPath')
      .send()
      .expect(404, done);
  });
});

describe('movements api', () => {
  it('should validate correct requests', done => {
    let movements = [
      {id: 1, date: '2020-10-05', amount: 10},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 3, date: '2021-02-01', amount: 20},
      {id: 4, date: '2021-03-31', amount: -30},
      {id: 5, date: '2021-04-08', amount: 40},
    ];

    let balances = [
      {date: '2020-01-31', balance: 70},
      {date: '2021-03-31', balance: 90},
      {date: '2021-01-31', balance: 100},
      {date: '2021-02-28', balance: 120},
      {date: '2021-05-31', balance: 130},
    ];

    request(app)
      .post('/movements/validation')
      .send({movements,balances})
      .expect('Content-Type', /json/)
      .expect(202, done);
  });

  it('should validate empty requests', done => {
    let movements = [];

    let balances = [];

    request(app)
      .post('/movements/validation')
      .send({movements,balances})
      .expect('Content-Type', /json/)
      .expect(202, done);
  });

  it('should fail on invalid requests, missing balances', done => {
    let movements = [
      {id: 1, date: '2020-10-05', amount: 10},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 3, date: '2021-02-01', amount: 20},
      {id: 4, date: '2021-03-31', amount: -30},
      {id: 5, date: '2021-04-08', amount: 40},
    ];

    request(app)
      .post('/movements/validation')
      .send({movements})
      .expect('Content-Type', /json/)
      .expect(401, done);
  });

  it('should fail on invalid requests, missing movements', done => {
    let balances = [
      {date: '2020-01-31', balance: 70},
      {date: '2021-03-31', balance: 90},
      {date: '2021-01-31', balance: 100},
      {date: '2021-02-28', balance: 120},
      {date: '2021-05-31', balance: 130},
    ];

    request(app)
      .post('/movements/validation')
      .send({balances})
      .expect('Content-Type', /json/)
      .expect(401, done);
  });

  it('should fail on inconsistent balances', done => {
    let balances = [
      {date: '2020-01-31', balance: 70},
      {date: '2021-03-31', balance: 90},
      {date: '2021-01-31', balance: 100},
      {date: '2021-02-28', balance: 120},
      {date: '2021-05-31', balance: 130},
    ];

    request(app)
      .post('/movements/validation')
      .send({balances, movements: []})
      .expect('Content-Type', /json/)
      .expect(418)
      .then(({body}) => {
        expect(body).toHaveProperty('message');
        expect(body).toHaveProperty('reasons');
        done();
      });
  });

  it('should fail on inconsistent movements', done => {
    let movements = [
      {id: 1, date: '2020-10-05', amount: 10},
      {id: 2, date: '2021-01-07', amount: 20},
      {id: 3, date: '2021-02-01', amount: 20},
      {id: 4, date: '2021-03-31', amount: -30},
      {id: 5, date: '2021-04-08', amount: 40},
    ];

    request(app)
      .post('/movements/validation')
      .send({balances: [], movements})
      .expect('Content-Type', /json/)
      .then(({body}) => {
        expect(body).toHaveProperty('message');
        expect(body).toHaveProperty('reasons');
        done();
      });
  });

});
