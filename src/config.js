// TODO: read config from env or config file.

let config = {
  logLevel: 'info',
  restApi: {
    port: 8080,
    logStyle: 'dev'
  }
};

export default config;
