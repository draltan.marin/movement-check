import express from 'express';
import app from './app.js';
import logger from './logger.js';
import config from './config.js';
import morgan from 'morgan';

// setup server
const server = express();
server.use(morgan(config.restApi.logStyle, { stream: logger.stream }));
server.use(app);

server.listen(config.restApi.port, () => {
  logger.info(`Server listening at http://localhost:${config.restApi.port}`);
}).on('error', (error) => {
  logger.error(`Unable to launch server. ${error}`);
  process.exit(1);
});
