import crypto from 'crypto';
import logger from './logger.js';

/*
Opération bancaire : { id: number, date: Date, wording: string, amount: number }
Point de contrôle : { date: Date, balance: number }
*/

// TODO verify data integrity and detect/handle ill formed data

// Makes sure date is a Date instance
export const normalizeDate = ({date, ...rest}) => {
  return {
    date: new Date(date),
    ...rest,
  };
};

export const aggregate = (movements) => {
  return movements.reduce((sum, movement) => {
    return sum + movement.amount;
  },0);
};

const movementHash = ({ id, date, wording, amount }) => {
  let shasum = crypto.createHash('sha1');
  id && shasum.update(id.toString());
  date && shasum.update(date.toString());
  wording && shasum.update(wording);
  amount && shasum.update(amount.toString());
  return shasum.digest('hex');
};

export const detectVariants = (movements) => {
  let variantsPerId = movements.reduce((acc, movement) => {
    let {id} = movement;
    let variants = acc[id] || {};
    let variantId = movementHash(movement);
    let { count = 0 } = variants[variantId] || {};
    // TODO should check movement contents as well, although
    // since variants are coomputed per id there is a negligeable
    // risk of collision.
    variants[variantId] = {
      count: count + 1,
      movement,
    };
    acc[id] = variants;
    return acc;
  }, {});

  return variantsPerId;
};

// Keeps one movement instance per variant
// possibly keeping inconsistent ids
export const ignoreDuplicates = (movements) => {
  let variantsPerId = detectVariants(movements);
  let result = [];
  Object.values(variantsPerId)
    .forEach(variants => {
      Object.values(variants).forEach(variant => {
        let {movement} = variant;
        result.push(movement);
      });
    });
  return result;
};

// Finds all duplicate movement ids and classifies them
// in either simple data duplication (same movement contents,
// single data variant),
// or id inconsistency (same id for different data, multiple
// data variants). Keeps count of duplication occurrences per
// variant.
export const detectDuplicates = (movements) => {
  let report = [];

  Object.values(detectVariants(movements))
    .forEach(variants => {
      if (Object.keys(variants).length === 1) {
        let {movement, count} = Object.values(variants)[0];

        if (count > 1) {
          report.push({
            type: 'DUPLICATE_MOVEMENTS',
            message: 'Duplicate movements',
            payload: {
              ...movement,
              count,
            }
          });
        }
      } else {
        report.push({
          type: 'INCONSISTENT_MOVEMENT_ID',
          message: 'Multiple variants found for the same movement id',
          payload: Object.values(variants)
        });
      }
    });
  return report;
};

export const periodBuilder = () => {
  let periods;
  let periodMovements;
  let startBalance;

  // Initializes a clean period builder state
  const init = () => {
    periods = [];
    periodMovements = [];
    startBalance = undefined;
  };

  // closes the current period by specifying a final balance
  const closePeriod = (balance) => {
    let period = {
      start: startBalance,
      end: balance,
      movements: periodMovements,
    };
    // avoid pushing empty periods
    if (
      (period.start !== undefined && period.end !== undefined) ||
      period.movements.length > 0
    ) {
      periods.push(period);
    }
    // ... and start a new one
    startBalance = balance;
    periodMovements = [];
  };

  // adds a new movement to the currently open period
  const addMovement = (currentMovement) => {
    periodMovements.push(currentMovement);
  };

  // returns extracted periods,
  // closing the final one if needed
  const getPeriods = () => {
    closePeriod(undefined);
    return periods;
  };

  init();
  return {
    init,
    closePeriod,
    addMovement,
    getPeriods,
  };
};

/*
  Groups a list of movements into separate periods
  defined by balances checkpoints' dates.

  Period intervals include movements strictly after the
  start date and up to including the end date.
  This is: start date < movement date <= end date

  Output periods are non overlapping and sorted in
  ascending date order.

  transactions = [
    {id: 1, date: 2020-10-05, amount: 10},
    {id: 2, date: 2021-01-07, amount: 20},
    {id: 3, date: 2021-02-01, amount: 20},
    {id: 4, date: 2021-03-15, amount: -30},
    {id: 5, date: 2021-04-08, amount: 40},
  ]

  balances = [
    {date: 2021-31-01, balance: 100},
    {date: 2021-28-02, balance: 120},
    {date: 2021-31-03, balance: 90},
  ]

  Expected result:
  periods: [
    {
      movements: {
        {id: 1, date: 2020-10-05, amount: 10},
        {id: 2, date: 2021-01-07, amount: 20},
      }
      start: undefined,
      end: {date: 2021-31-01, balance: 100},
    },
    {
      movements: {
        {id: 3, date: 2021-02-01, amount: 20},
      }
      start: {date: 2021-31-01, balance: 100},
      end: {date: 2021-28-02, balance: 120},
    },
    {
      movements: {
        {id: 4, date: 2021-03-15, amount: -30},
      }
      start: {date: 2021-28-02, balance: 120},
      end: {date: 2021-31-03, balance: 90},
    },
    {
      movements: {
        {id: 5, date: 2021-04-08, amount: 40},
      }
      start: {date: 2021-31-03, balance: 90},
      end: undefined,
    },
  ]
*/
export const resolvePeriods = ({movements, balances}) => {
  // Sort balances and movements (avoid undefined or falsy values)
  let sortedBalances = balances
    .filter(balance => balance) // remove falsy values
    .sort((a, b) => a.date - b.date);
  let sortedMovements = movements
    .filter(movement => movement) // remove falsy values
    .sort((a, b) => a.date - b.date);

  // Iterate over movements and balances in order
  // to produce all consecutive periods with the
  // associated movements.
  let i = 0;
  let j = 0;
  let periods = periodBuilder();

  while ( // go through all movements and all balances
    i < sortedBalances.length ||
    j < sortedMovements.length
  ) {
    // The following are undefined if out of range
    // At least one of the following is not undefined
    let currentBalance = sortedBalances[i];
    let currentMovement = sortedMovements[j];

    if (currentBalance !== undefined) {
      if (currentMovement !== undefined) {
        // keep all movements up to or equal to balance date
        // close the period otherwise
        if (currentBalance.date < currentMovement.date) {
          periods.closePeriod(currentBalance);
          // Move balance index forward
          i++;
        } else {
          periods.addMovement(currentMovement);
          // move movements index forward
          j++;
        }
      } else { // no more movements
        periods.closePeriod(currentBalance);
        // Move balance index forward
        i++;
      }
    } else { // no more balances
      if (currentMovement !== undefined) {
        periods.addMovement(currentMovement);
        // move movements index forward
        j++;
      } else { // no more movements, no more balances
        logger.error('Invariant violation: should have at least a balance or a movement');
        break;
      }
    }
  }

  return periods.getPeriods();
};

export const checkPeriod = ({movements, start, end}) => {
  let report = detectDuplicates(movements);

  let movementsTotal = aggregate(movements);

  let initialBalance = 0;
  if (start) {
    initialBalance = start.balance;
  } else {
    report.push({
      type: 'NO_INITIAL_BALANCE',
      message: 'No initial balance found for period. Assuming zero as initial balance.',
    });
  }

  let expectedFinalBalance = initialBalance + movementsTotal;

  if (!end) {
    report.push({
      type: 'NO_FINAL_BALANCE',
      message: 'No final balance found for period. Unable to check final balance.',
      payload: {
        initialBalance,
        movementsTotal,
        expectedFinalBalance,
        actualFinalBalance: undefined,
      }
    });
  } else {
    if (expectedFinalBalance !== end.balance) {
      report.push({
        type: 'BALANCE_MISMATCH',
        message: 'Period balance mismatch',
        payload: {
          initialBalance,
          movementsTotal,
          expectedFinalBalance,
          actualFinalBalance: end.balance,
          missing: end.balance - expectedFinalBalance,
        }
      });
    }
  }
  return report;
};

export const hasErrors = (report) => {
  let {duplicateMovements, periods} = report;
  return duplicateMovements.length > 0 || periods.length > 0;
};

export const checkBalances = ({movements, balances, skipDuplicates = false}) => {
  let normalizedMovements = movements.map(normalizeDate);
  let normalizedBalances = balances.map(normalizeDate);

  let duplicateMovements = detectDuplicates(normalizedMovements);

  let resoledMovements = normalizedMovements;
  if (skipDuplicates) {
    resoledMovements = ignoreDuplicates(normalizedMovements);
  }

  let periods = resolvePeriods({
    movements: resoledMovements,
    balances: normalizedBalances,
  });

  let issues = [];
  periods.forEach(period => {
    let issueList = checkPeriod(period);
    if (issueList.length) {
      let {start, end} = period;
      let periodStart = start ? start.date: null;
      let periodEnd = end ? end.date: null;
      issues.push({
        periodStart,
        periodEnd,
        issueList
      });
    }
  });

  return {
    duplicateMovements,
    periods: issues,
  };
};
